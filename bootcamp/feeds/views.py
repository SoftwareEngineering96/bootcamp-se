import json

from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseForbidden)
from django.shortcuts import get_object_or_404, render
from django.template.context_processors import csrf
from django.template.loader import render_to_string

from bootcamp.activities.models import Activity
from bootcamp.decorators import ajax_required
from bootcamp.feeds.models import Feed, BaseFeed, Repost
from bootcamp.feeds.models import Report

FEEDS_NUM_PAGES = 10


@login_required
def feeds(request):
    all_feeds = BaseFeed.get_feeds()
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    feeds = paginator.page(1)
    from_feed = -1
    if feeds:
        from_feed = feeds[0].id
    return render(request, 'feeds/feeds.html', {
        'feeds': feeds,
        'from_feed': from_feed,
        'page': 1,
        })


def feed(request, pk):
    feed = get_object_or_404(Feed, pk=pk)
    return render(request, 'feeds/feed.html', {'feed': feed})


@login_required
@ajax_required
def load(request):
    from_feed = request.GET.get('from_feed')
    page = request.GET.get('page')
    feed_source = request.GET.get('feed_source')
    all_feeds = BaseFeed.get_feeds(from_feed)
    if feed_source != 'all':
        all_feeds = all_feeds.filter(user__id=feed_source)
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    try:
        feeds = paginator.page(page)
    except PageNotAnInteger:
        return HttpResponseBadRequest()
    except EmptyPage:
        feeds = []
    html = ''
    csrf_token = (csrf(request)['csrf_token'])
    for feed in feeds:
        if feed.is_repost():
            html = '{0}{1}'.format(html,
                                   render_to_string('feeds/partial_repost.html',
                                                    {
                                                        'feed': feed,
                                                        'user': request.user,
                                                        'csrf_token': csrf_token
                                                    }))
        else:
            html = '{0}{1}'.format(html,
                                   render_to_string('feeds/partial_feed.html',
                                                    {
                                                        'feed': feed.get_feed(),
                                                        'user': request.user,
                                                        'csrf_token': csrf_token
                                                    }))

    return HttpResponse(html)


def _html_feeds(last_feed, user, csrf_token, feed_source='all'):
    feeds = BaseFeed.get_feeds_after(last_feed)
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)
    html = ''
    for feed in feeds:
        if feed.is_repost():
            html = '{0}{1}'.format(html,
                                   render_to_string('feeds/partial_repost.html',
                                                    {
                                                        'feed': feed,
                                                        'user': user,
                                                        'csrf_token': csrf_token
                                                    }))
        else:
            html = '{0}{1}'.format(html,
                                   render_to_string('feeds/partial_feed.html',
                                                    {
                                                        'feed': feed.get_feed(),
                                                        'user': user,
                                                        'csrf_token': csrf_token
                                                    }))

    return html


@login_required
@ajax_required
def load_new(request):
    last_feed = request.GET.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    html = _html_feeds(last_feed, user, csrf_token)
    return HttpResponse(html)


@login_required
@ajax_required
def check(request):
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feeds = BaseFeed.get_feeds_after(last_feed)
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)

    count = feeds.count()
    return HttpResponse(count)


@login_required
@ajax_required
def post(request):
    last_feed = request.POST.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    feed = Feed()
    feed.user = user
    post = request.POST['post']
    post = post.strip()
    if len(post) > 0:
        feed.post = post[:255]
        feed.save()
    html = _html_feeds(last_feed, user, csrf_token)
    return HttpResponse(html)


@login_required
@ajax_required
def like(request):
    feed_id = request.POST['feed']
    feed = BaseFeed.objects.get(pk=feed_id)
    main_feed = feed
    if feed.is_repost():
        main_feed = feed.source

    user = request.user
    like = Activity.objects.filter(activity_type=Activity.LIKE, feed=main_feed.id,
                                   user=user)
    if like:
        user.profile.unotify_liked(main_feed)
        like.delete()

    else:
        like = Activity(activity_type=Activity.LIKE, feed=main_feed.id, user=user)
        like.save()
        user.profile.notify_liked(main_feed)

    return HttpResponse(main_feed.calculate_likes())


@login_required
@ajax_required
def repost(request):
    feed_id = request.POST['feed']
    feed = BaseFeed.objects.get(pk=feed_id)
    main_feed = feed
    if feed.is_repost():
        main_feed = feed.source
    user = request.user
    repost = Repost.objects.filter(source=main_feed, user=user)

    if user in main_feed.get_reposters():
        user.profile.unotify_reposted(main_feed)
        repost.delete()

    else:
        repost = Repost.objects.create(source=main_feed, user=user)
        repost.save()
        user.profile.notify_reposted(main_feed)

    return HttpResponse(main_feed.calculate_reposts())

@login_required
@ajax_required
def comment(request):
    if request.method == 'POST':
        feed_id = request.POST['feed']
        feed = BaseFeed.objects.get(pk=feed_id)
        main_feed = feed
        if feed.is_repost():
            main_feed = feed.source

        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            post = post[:255]
            user = request.user
            main_feed.comment(user=user, post=post)
            user.profile.notify_commented(main_feed)
            user.profile.notify_also_commented(main_feed)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed.get_feed()})

    else:
        feed_id = request.GET.get('feed')
        feed = BaseFeed.objects.get(pk=feed_id)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed.get_feed()})


@login_required
@ajax_required
def report(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id)
    if feed.is_repost():
        feed = feed.source
    user = request.user

    if feed.user == user or user in feed.get_reporters():
        return HttpResponseBadRequest()

    rep = Report(user=user, feed=feed)
    rep.save()

    response_data = {}
    if feed.get_reports() >= 5:
        response_data['removePost'] = 'true'
        Feed.objects.get(pk=feed_id).delete()
        Report.objects.filter(feed=feed_id).delete()
    else:
        response_data['removePost'] = 'false'
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@login_required
@ajax_required
def pin(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id)
    user = request.user
    if feed.user != user:
        return HttpResponseBadRequest()
    if user.profile.pinned_feed is None:
        user.profile.pinned_feed = feed
    else:
        if user.profile.pinned_feed == feed:
            user.profile.pinned_feed = None
        else:
            user.profile.pinned_feed = feed
    user.profile.save()
    return HttpResponse()


@login_required
@ajax_required
def update(request):
    first_feed = request.GET.get('first_feed')
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feeds = BaseFeed.get_feeds().filter(id__range=(last_feed, first_feed))
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)
    dump = {}
    for feed in feeds:
        if feed.is_repost():
            dump[feed.pk] = {'likes': feed.source.likes, 'comments': feed.source.comments, 'reposts': feed.source.reposts}
        else:
            dump[feed.pk] = {'likes': feed.likes, 'comments': feed.comments, 'reposts': feed.reposts}
    data = json.dumps(dump)
    return HttpResponse(data, content_type='application/json')


@login_required
@ajax_required
def track_comments(request):
    feed_id = request.GET.get('feed')
    feed = BaseFeed.objects.get(pk=feed_id)
    if feed.is_repost():
        feed = feed.source
    if len(feed.get_comments()) > 0:
        return render(
            request, 'feeds/partial_feed_comments.html', {'feed': feed})

    else:
        return HttpResponse()


@login_required
@ajax_required
def remove(request):
    try:
        feed_id = request.POST.get('feed')
        feed = BaseFeed.objects.get(pk=feed_id)
        if not feed.is_repost():
            if feed.user == request.user:
                likes = feed.get_likes()
                parent = feed.parent
                for like in likes:
                    like.delete()
                feed.delete()
                if parent:
                    parent.calculate_comments()
                return HttpResponse()
            else:
                return HttpResponseForbidden()
        else:
            source = feed.source
            feed.delete()
            source.calculate_reposts()
            return HttpResponse()

    except Exception:
        return HttpResponseBadRequest()
