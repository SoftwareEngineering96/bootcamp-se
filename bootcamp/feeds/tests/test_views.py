from django.contrib.auth import get_user_model
from django.http import HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.test import Client, TestCase

from bootcamp.feeds.models import Feed, Report


class TestViews(TestCase):
    """
    Includes tests for all the functionality
    associated with Views
    """

    def setUp(self):
        self.client = Client()
        self.other_client = Client()
        self.client3 = Client()
        self.client4 = Client()
        self.client5 = Client()
        self.client6 = Client()
        self.user = get_user_model().objects.create_user(
            username='test_user',
            email='test@gmail.com',
            password='top_secret'
        )
        self.other_user = get_user_model().objects.create_user(
            username='other_test_user',
            email='other_test@gmail.com',
            password='top_secret'
        )
        self.user3 = get_user_model().objects.create_user(
            username='user3',
            email='user3@gmail.com',
            password='top_secret'
        )
        self.user4 = get_user_model().objects.create_user(
            username='user4',
            email='user4@gmail.com',
            password='top_secret'
        )
        self.user5 = get_user_model().objects.create_user(
            username='user5',
            email='user5@gmail.com',
            password='top_secret'
        )
        self.user6 = get_user_model().objects.create_user(
            username='user6',
            email='user6@gmail.com',
            password='top_secret'
        )

        self.client.login(username='test_user', password='top_secret')
        self.other_client.login(
            username='other_test_user', password='top_secret')
        self.client3.login(username='user3', password='top_secret')
        self.client4.login(username='user4', password='top_secret')
        self.client5.login(username='user5', password='top_secret')
        self.client6.login(username='user6', password='top_secret')

        self.feed = Feed.objects.create(
            user=self.user,
            post='A not so long text',
            likes=0,
            comments=0)

    def test_feed_view(self):
        request = self.client.get(reverse('feed', args=[self.feed.id]))
        self.assertEqual(request.status_code, 200)

    def test_feed_pin(self):
        self.assertEqual(self.user.profile.pinned_feed, None)
        request = self.client.post(reverse('pin'),
                                   {'feed': self.feed.id},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request_not_my_feed = self.other_client.post(reverse('pin'),
                                                     {'feed': self.feed.id},
                                                     HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request_not_my_feed.status_code, 400)
        request = self.client.post(reverse('pin'),
                                   {'feed': self.feed.id},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        self.assertEqual(self.user.profile.pinned_feed, None)

    def test_feed_report_twice(self):
        request = self.other_client.post(reverse('report'),
                                         {'feed': self.feed.id},
                                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request_repeated = self.other_client.post(reverse('report'),
                                                  {'feed': self.feed.id},
                                                  HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request_repeated.status_code, 400)

    def test_feed_report_five_times(self):
        request = self.other_client.post(reverse('report'),
                                         {'feed': self.feed.id},
                                         HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request = self.client3.post(reverse('report'),
                                    {'feed': self.feed.id},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request = self.client4.post(reverse('report'),
                                    {'feed': self.feed.id},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request = self.client5.post(reverse('report'),
                                    {'feed': self.feed.id},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)
        request = self.client6.post(reverse('report'),
                                    {'feed': self.feed.id},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 200)

        self.assertFalse(Report.objects.filter(feed=self.feed.id).exists())

        self.assertFalse(Feed.objects.filter(pk=self.feed.id).exists())

    def test_report_self(self):
        request = self.client.post(reverse('report'),
                                   {'feed': self.feed.id},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(request.status_code, 400)
