from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

import bleach
from bootcamp.activities.models import Activity
from polymorphic.models import PolymorphicModel


class BaseFeed(PolymorphicModel):
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)

    def is_repost(self):
        pass

    def get_feed(self):
        pass

    @staticmethod
    def newest_feed(feed, feeds_list):
        for f in feeds_list:
            if feed.get_feed() == f.get_feed() and f.date > feed.date:
                return False
        return True

    @staticmethod
    def get_feeds(from_feed=None):
        if from_feed is not None:
            base_feeds = BaseFeed.objects.filter(id__lte=from_feed)
        else:
            base_feeds = BaseFeed.objects.filter()
        feeds_list = [b for b in base_feeds if (not b.is_repost() and b.get_feed().parent is None) or b.is_repost()]
        feeds_list = [feed.id for feed in feeds_list if BaseFeed.newest_feed(feed, feeds_list)]
        feeds = base_feeds.filter(id__in=feeds_list).order_by('-date')
        return feeds

    @staticmethod
    def get_feeds_after(feed):
        base_feeds = BaseFeed.objects.filter(id__gt=feed)
        feeds_list = [b for b in base_feeds if (not b.is_repost() and b.get_feed().parent is None) or b.is_repost()]
        feeds_list = [feed.id for feed in feeds_list if BaseFeed.newest_feed(feed, feeds_list)]
        feeds = base_feeds.filter(id__in=feeds_list).order_by('-date')
        return feeds


@python_2_unicode_compatible
class Feed(BaseFeed):
    post = models.TextField(max_length=255)
    parent = models.ForeignKey('Feed', null=True, blank=True)
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    reposts = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ('-date',)

    def is_repost(self):
        return False

    def get_feed(self):
        return self

    def __str__(self):
        return self.post

    def get_comments(self):
        return Feed.objects.filter(parent=self).order_by('date')

    def calculate_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk).count()
        self.likes = likes
        self.save()
        return self.likes

    def get_likes(self):
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk)
        return likes

    def get_likers(self):
        likes = self.get_likes()
        likers = []
        for like in likes:
            likers.append(like.user)
        return likers

    def get_reports(self):
        return Report.objects.filter(feed=self).count()

    def get_reporters(self):
        reports = Report.objects.filter(feed=self)
        reporters = []
        for report in reports:
            reporters.append(report.user)
        return reporters

    def get_reposts(self):
        return Repost.objects.filter(source=self)

    def get_reposters(self):
        reposts = Repost.objects.filter(source=self)
        reposters = []
        for repost in reposts:
            reposters.append(repost.user)
        return reposters

    def calculate_reposts(self):
        reposts = Repost.objects.filter(source=self).count()
        self.reposts = reposts
        self.save()
        return self.reposts

    def calculate_comments(self):
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return self.comments

    def comment(self, user, post):
        feed_comment = Feed(user=user, post=post, parent=self)
        feed_comment.save()
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return feed_comment

    def linkfy_post(self):
        return bleach.linkify(escape(self.post))


class Repost(BaseFeed):
    source = models.ForeignKey(Feed, on_delete=models.CASCADE)

    def is_repost(self):
        return True

    def get_feed(self):
        return self.source


class Report(models.Model):
    user = models.ForeignKey(User)
    feed = models.ForeignKey(Feed)

    class Meta:
        unique_together = (('user', 'feed'),)
